---
Title: Get In Touch
---
# Get In Touch

It seems you have thought of the most excellent plan of reaching out to us! Any flowers and love letters are best not sent but delivered in person at the next gig. They will will be highly appreciated. 

In the unlikely occasion that you have another type of inquiry there are many options in the 21st century, some common and easy ways are: 

1. Phone -->  06 2141 7331
2. Mail  -->  aksi (at) AntiforZ.com
3. Book us on [GigStarter](https://www.gigstarter.nl/artists/antiforz)
4. Sing a song in the wind and be patient!
