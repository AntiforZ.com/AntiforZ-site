---
title: About
---
# About 

 AntiforZ is here for fresh sounds of the block! Or as we call it organic dance music from our local galaxy!

 The songs are brewed by the chef du fromage Chai and then played and brought to you by Rotterdam's finest musicians: Anne, Jos, Julien, Famke, Linda and Marcel. 

 Each one of these handsome devils is worth a page of their own, and in due time may actually get one. For now this is it and if you insist you should learn more, which we encourage, please hang out with us at the next [gig!](/gigs.html)


