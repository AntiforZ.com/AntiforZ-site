module Main where

import GHC.Generics
import SitePipe
import System.Directory
import qualified Text.Mustache as MT
import qualified Text.Mustache.Types as MT
import qualified Data.Text as T
import Data.List (sort)
 
data Media = Media {src :: String, url :: String} deriving (Show, Eq, Ord, Generic)
data ImageGallery = ImageGallery { name :: String, images :: [Media] } deriving (Show,Generic)
data ImageGalleries = ImageGalleries { galleries :: [ImageGallery], url :: String } deriving (Show, Generic)
instance ToJSON ImageGalleries
instance ToJSON ImageGallery
instance ToJSON Media 
--instance MT.ToMustache Media where 
--  {
--    MT.toMustache media = object
--      [ "src" MT.~> src (media :: Media)
--      , "url" MT.~> url (media :: Media)
--      ]   
--  }

instance FromJSON Media 
data AudioGallery = AudioGallery { name :: String, songs :: [Media] } deriving (Show,Generic)
data AudioGalleries = AudioGalleries { galleries :: [AudioGallery], url :: String } deriving (Show, Generic)
instance ToJSON AudioGalleries
instance ToJSON AudioGallery

main :: IO ()
main
 =
  site $ do
    pages <- resourceLoader markdownReader ["*.md"]
    _ <- staticAssets
    -- Next is about image galleries
    names_ <- liftIO $ listDirectory "site/images/gallery/"
    let names = filter (\x -> (head x) /= '.') names_
    let imgGlobs = fmap (\x -> "images/gallery/" ++ x ++ "/*" ) names
         
    imgs <- traverse (\x -> copyFiles [x])  imgGlobs
    let imgs' =  fmap asImages imgs where
          asImages :: [Value] -> [Media]
          asImages x = case traverse fromJSON x of
            Success i -> i
            Error err -> error ( "Unable to create image gallery: " ++ err ) 
    let galleriesWithNames = zip imgs' names   
        imageGalleryList = fmap (\(i,n) -> ImageGallery { name = n, images = i }) galleriesWithNames    
        imageGalleries = ImageGalleries { galleries = imageGalleryList, url = "photos.html"}
    
    -- Next is about music  galleries
    songNames_ <- liftIO $ listDirectory "site/audio/"
    let songNames = filter (\x -> (head x) /= '.') songNames_
    let audioGlobs = fmap (\x -> "audio/" ++ x ++ "/*.mp3" ) songNames
         
    audioFiles <- traverse (\x -> copyFiles [x])  audioGlobs
    let audioFiles' =  fmap asMedias audioFiles where
          asMedias :: [Value] -> [Media]
          asMedias x = case traverse fromJSON x of
            Success i -> i
            Error err -> error ( "Unable to create music gallery: " ++ err ) 
        audioFiles'' = (fmap . fmap) (\x -> x { src = (takeBaseName (src x))} ) audioFiles'
        galleriesWithNames = zip audioFiles'' songNames   
        audioGalleryList = fmap (\(s,n) -> AudioGallery { name = n, songs = s }) galleriesWithNames    
        audioGalleries = AudioGalleries { galleries = audioGalleryList, url = "music.html"}
    
    --- Next is about the gigs history list
    gigNames_ <- liftIO $ listDirectory "site/gigs/"
    let gigNames =  filter (\x -> (head x) /= '.' && x /= "next_gig.jpg" ) $  gigNames_
        gigGlobs = fmap (\x -> "gigs/" ++ x ++ "/*") gigNames
    
    gigFiles <- traverse (\x -> copyFiles [x]) gigGlobs
    let gigFiles'' = (fmap sort ) .  (fmap asImages) $ gigFiles where
          asImages :: [Value] -> [Media]
          asImages x = case traverse fromJSON x of
            Success i -> i
            Error err -> error ( "Unable to create image gallery: " ++ err ) 
    let gigFiles' = (reverseSnd . reverse ) $ gigFiles'' where 
          reverseSnd (x:xs) = reverse $ (reverse x):xs
    let gigFiles = (fmap . fmap) (\x -> x {src = getName (url (x :: Media))}) $ gigFiles'
    let gigGalleriesWithNames = zip gigFiles gigNames  
        gigGalleryList = fmap (\(i,n) -> ImageGallery { name = n, images = i }) gigGalleriesWithNames    
        gigGalleries = ImageGalleries { galleries = gigGalleryList, url = "gigs.html"}
    --- Next: writing the templates:
    writeTemplate "templates/pages.html"  pages
    writeTemplate "templates/photos.html" [imageGalleries]
    writeTemplate "templates/gigs.html" [gigGalleries]
    writeTemplate "templates/music.html" [audioGalleries]

staticAssets :: SiteM [Value]
staticAssets =
  copyFiles
    [ "css/"
    , "js/"
    , "images/"
    , "audio/"
    , "gigs/"
    , "downloads/"
    ]

templateFuncs :: MT.Value
templateFuncs = MT.object
  [ "toName" MT.~> MT.overText (T.pack . getName . T.unpack)
  ]

toUrl' :: String -> String -> String
toUrl' dir  = (++) dir

getName :: String -> String
getName = takeBaseName
